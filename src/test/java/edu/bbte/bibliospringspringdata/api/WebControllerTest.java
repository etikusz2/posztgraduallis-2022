package edu.bbte.bibliospringspringdata.api;

import edu.bbte.bibliospringspringdata.assemblers.BookAssembler;
import edu.bbte.bibliospringspringdata.assemblers.UserAssembler;
import edu.bbte.bibliospringspringdata.dto.incoming.BookInDTO;
import edu.bbte.bibliospringspringdata.dto.incoming.UserInDTO;
import edu.bbte.bibliospringspringdata.dto.outgoing.BookOutDTO;
import edu.bbte.bibliospringspringdata.model.Author;
import edu.bbte.bibliospringspringdata.model.Book;
import edu.bbte.bibliospringspringdata.model.User;
import edu.bbte.bibliospringspringdata.service.BookService;
import edu.bbte.bibliospringspringdata.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


class WebControllerTest {
    private MockMvc mockMvc;
    @InjectMocks
    private WebController webController;

    @Mock
    private BookService bookService;
    @Mock
    private UserService userService;
    @Mock
    private BookAssembler bookAssembler;
    @Mock
    private UserAssembler userAssembler;
    @Mock
    private HttpSession session;
    @Mock
    private Model model;
    @Mock
    private BindingResult bindingResult;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        webController = new WebController(bookService, userService, bookAssembler, userAssembler);
        mockMvc = MockMvcBuilders.standaloneSetup(webController).build();
    }

    @Test
    void testLoginSuccess() {
        UserInDTO userDTO = new UserInDTO();
        userDTO.setUsername("user");
        userDTO.setPassword("password");

        when(userService.login(userDTO.getUsername(), userDTO.getPassword())).thenReturn(true);

        String viewName = webController.login(userDTO, session, model);

        assertEquals("redirect:/books", viewName, "With login success the view redirecting to books");
        verify(session).setAttribute(eq("loggedInUser"), any(UserInDTO.class));
    }

    @Test
    void testLoginFailure() {
        UserInDTO userDTO = new UserInDTO();
        userDTO.setUsername("user");
        userDTO.setPassword("wrongpassword");

        when(userService.login(userDTO.getUsername(), userDTO.getPassword())).thenReturn(false);

        String viewName = webController.login(userDTO, session, model);

        assertEquals("login", viewName, "login should be failure");
        verify(model).addAttribute(eq("loginError"), anyString());
    }

    @Test
    void givenBookswhenListingBooksthenShouldAddBooksToModel() {
        List<Book> books = List.of(new Book());
        when(bookService.getAllBooksSortedByAuthor()).thenReturn(books);

        BookOutDTO bookOutDTO = new BookOutDTO();
        when(bookAssembler.modelToBookOutDto(any(Book.class))).thenReturn(bookOutDTO);

        Model model = mock(Model.class);

        WebController controller = new WebController(bookService, userService, bookAssembler, userAssembler);
        String viewName = controller.showBooks(model);

        verify(model).addAttribute(eq("books"), anyList());
        verify(bookService).getAllBooksSortedByAuthor();

        assertEquals("books", viewName, "This is the books view");
    }


    @Test
    void showEditBookFormBookExistsShouldAddBookToModel() {
        Long bookId = 1L;
        Book book = new Book();
        book.setId(bookId);
        when(bookService.getById(bookId)).thenReturn(book);

        BookOutDTO bookOutDTO = new BookOutDTO();
        when(bookAssembler.modelToBookOutDto(book)).thenReturn(bookOutDTO);

        String viewName = webController.showEditBookForm(bookId, model);

        assertThat(viewName).isEqualTo("editbook");
        verify(model).addAttribute("book", bookOutDTO);
        verify(bookService).getById(bookId);
    }

    @Test
    void showRegisterFormShouldAddNewUserToModel() {
        webController.showRegisterForm(model);

        verify(model).addAttribute(eq("newUser"), any(UserInDTO.class));

        verify(model).addAttribute(eq("newUser"), isA(UserInDTO.class));
    }


    @Test
    void registerUserWithErrorsShouldReturnRegisterView() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = webController.registerUser(new UserInDTO(), bindingResult, model);

        assertThat(viewName).isEqualTo("register");
        verify(bindingResult).hasErrors();
    }

    @Test
    void showAddBookFormShouldAddBookToModel() {
        String viewName = webController.showAddBookForm(model);

        assertThat(viewName).isEqualTo("addbook");
        verify(model).addAttribute(eq("book"), any(BookInDTO.class));
    }

    @Test
    void addBookWithErrorsShouldReturnAddBookView() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = webController.addBook(new BookInDTO(), bindingResult);

        assertThat(viewName).isEqualTo("addbook");
        verify(bindingResult).hasErrors();
    }

    @Test
    void showEditBookFormBookDoesNotExistShouldRedirectToError() {
        Long bookId = 1L;
        when(bookService.getById(bookId)).thenReturn(null);

        String viewName = webController.showEditBookForm(bookId, model);

        assertThat(viewName).isEqualTo("redirect:/error");
        verify(bookService).getById(bookId);
    }

    @Test
    void editBookWithErrorsShouldReturnEditBookView() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = webController.editBook(1L, new BookInDTO(), bindingResult);

        assertThat(viewName).isEqualTo("editbook");
        verify(bindingResult).hasErrors();
    }

    @Test
    void deleteBookShouldRedirectToBooks() {
        Long bookId = 1L;

        String viewName = webController.deleteBook(bookId);

        assertThat(viewName).isEqualTo("redirect:/books");
        verify(bookService).deleteById(bookId);
    }

    @Test
    void testAddUserSuccess() {
        UserInDTO newUserDTO = new UserInDTO();
        newUserDTO.setUsername("testUser");
        newUserDTO.setPassword("password");

        User newUser = new User();
        newUser.setUsername(newUserDTO.getUsername());
        newUser.setPassword(newUserDTO.getPassword());

        when(userAssembler.userInDtoToModel(any(UserInDTO.class))).thenReturn(newUser);

        BindingResult result = new BeanPropertyBindingResult(newUserDTO, "newUser");

        String viewName = webController.registerUser(newUserDTO, result, new ExtendedModelMap());

        verify(userService).create(any(User.class));
        assertEquals("redirect:/login", viewName,
                "After created a new user redirect to the login view");
    }

    @Test
    void testSearchBooksByTitle() throws Exception {
        List<Book> booksWithTitle = new ArrayList<>();
        when(bookService.findByTitleStartingWith("testTitle")).thenReturn(booksWithTitle);

        mockMvc.perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/search")
                                .param("title", "testTitle"))
                .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers
                        .status().isOk())
                .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.view()
                        .name("searchResults"))
                .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.model()
                        .attributeExists("books"));

        verify(bookService).findByTitleStartingWith("testTitle");
    }

    @Test
    void testSearchBooksByAuthorName() throws Exception {
        List<Author> authorsWithName = new ArrayList<>();
        List<Book> booksByAuthor = new ArrayList<>();

        when(bookService.findByAuthorNameStartingWith("testAuthor")).thenReturn(authorsWithName);
        when(bookService.findBooksByAuthor(any(Author.class))).thenReturn(booksByAuthor);

        mockMvc.perform(
                        org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/search")
                                .param("authorName", "testAuthor"))
                .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk())
                .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.view()
                        .name("searchResults"))
                .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.model()
                        .attributeExists("books"));

        verify(bookService).findByAuthorNameStartingWith("testAuthor");
    }
}
