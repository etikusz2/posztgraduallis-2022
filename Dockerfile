# FROM gradle:8.5-jdk17-jammy
# WORKDIR /workdir
# COPY . ./
# RUN gradle build -x check
# CMD gradle botRun

FROM eclipse-temurin:17.0.10_7-jre-jammy
COPY build/libs/BibliospringSpringData-*.jar ./
CMD java -jar BibliospringSpringData-*.jar


